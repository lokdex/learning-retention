import time
from threading import Thread

import WonderPy.core.wwMain
from WonderPy.core.wwConstants import WWRobotConstants
from WonderPy.core.wwCommands import *


class MyClass(object):

	def on_connect(self, robot):
		print "Starting a thread for %s." % (robot.name)
		Thread(target=self.thread_parabolico, args=(robot,)).start()

	
	def thread_parabolico(self, robot):
		print "\tPARABOLIC MOTION\n"
		while (potencia != -1):
			if(potencia == 0):
				print "Type the power for the launcher:\n"
			else:
				robot.commands.accessory.do_launcher_launch(potencia)
				print "Throwing with %d power" % (potencia)
			if(potencia == -1):
				break

if __name__ == "__main__":
	WonderPy.core.wwMain.start(MyClass())