# -*- coding: utf-8 -*-
import time
from math import sqrt
from threading import Thread
import WonderPy.core.wwMain
from WonderPy.core.wwConstants import WWRobotConstants

class MyClass(object):

    def move_constant(self, robot, dist, speed):
        print(u"%s driving forward %s cm at %s cm/s." % (robot.name, dist, speed))
        robot.cmds.body.do_forward(dist, speed)
        print(u"%s turning body around to the right." % (robot.name))
        robot.cmds.body.do_turn(-180, 200)
    
    def move_accel(self, robot, dist, speed, accel):
        print(u"%s driving forward %s cm at %s cm/s with %s cm(s2)." % (robot.name, dist, speed, accel))
        ctrl = (sqrt(speed**2+2*accel*dist)-speed)/accel
        start = time.time()
        end = time.time()
        while(True):
            if(float(end)-float(start) < ctrl):
                robot.cmds.body.stage_wheel_speeds(speed, speed)
                time.sleep(0.1)
                speed += (accel/10)
                end = time.time()
            else:
                robot.cmds.body.stage_wheel_speeds(0, 0)
                break
        time.sleep(3)
        robot.cmds.body.do_turn(-180, 200)

    def move_accel_var(self, robot, dist, speed, accel, var):
        print(u"%s driving forward %s cm at %s cm/s with %s cm(s2) with an increase of %s in the acceleration." % (robot.name, dist, speed, accel, var))
        ctrl = sqrt((2*(dist-speed))/accel)
        start = time.time()
        end = time.time()
        while(True):
            if(float(end)-float(start) < ctrl):
                robot.cmds.body.stage_wheel_speeds(speed, speed)
                time.sleep(0.1)
                speed += (accel/10)
                end = time.time()
            else:
                break
        robot.commands.body.stage_stop()


    def on_connect(self, robot):
        print("Starting a thread for %s." % (robot.name))
        Thread(target=self.thread_mover, args=(robot,)).start()
        
    def thread_mover(self, robot):
        if not robot.has_ability(WWRobotConstants.WWRobotAbilities.BODY_MOVE, True):
            print(u"%s cannot drive!" % (robot.name))
            return

        modo = 0
        while(modo != 4):
            print "\tTESTING MENU\n"
            print "1. Linear motion without acceleration\n"
            print "2. Linear moiton with constant acceleration\n"
            print "3. Linear motion with varying acceleration\n"
            print "4. Exit\n"
            modo = raw_input("Choose an option:\n")
            if (modo == "1"):
                print "\n\tLINEAR MOTION WITHOUT ACCELERATION"
                dist = input("Type the distance:\n")
                speed = input("Type the speed;\n")
                self.move_constant(robot, dist, speed)
            elif (modo == "2"):
                print "\n\tCONSTANT ACCELERATION"
                dist = input("Type the distance:\n")
                speed = input("Type the speed;\n")
                accel = input("Type the acceleration:\n")
                self.move_accel(robot, dist, speed, accel)
            elif (modo == "3"):
                print "\n\tACELERACION VARIABLE"
                dist = input("Type the distance:\n")
                speed = input("Type the speed;\n")
                accel = input("Type the acceleration:\n")
                var = input("Type the variation for the acceleration:\n")
                #self.move_accel(self, robot, dist, speed, accel)
                self.move_accel_var(self, robot, dist, speed, accel, var)
                time.sleep(2)
                self.robot.commands.body.stage_stop()

                    
if __name__ == "__main__":
    modo = 0
    WonderPy.core.wwMain.start(MyClass())